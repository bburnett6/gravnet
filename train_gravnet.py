import numpy as np 
import matplotlib.pyplot as plt 
import time
import math
import sys
import random
import os
import glob

import torch
import torch.nn as nn 
import torch.nn.functional as F 
from torch.autograd import Variable 

seed = 1234
torch.manual_seed(seed)
random.seed(seed)
np.random.seed(seed)

####################
# Helper functions
####################
def time_since(start_time):
	s = time.time() - start_time
	m = math.floor(s / 60)
	s -= m * 60
	return f"{m}m {s:.2f}s"

def find_minmax(*args):
	total_max = sys.float_info.min 
	total_min = sys.float_info.max 
	for a in args:
		total_max = max(total_max, np.max(a))
		total_min = min(total_min, np.min(a))

	return total_min, total_max

def file_to_tensor(fname):
	#torch.tensor(list(map(lambda x : float(x), f.readline().split(',')[:-1]))).view(n_p, 2)
	with open(fname, 'r') as f:
		for i, line in enumerate(f.readlines()):
			ls = list(map(lambda x : float(x), line.split(',')[:-1]))
			if i == 0:
				ten = torch.tensor(ls)
			elif i == 1:
				ten = torch.stack((ten, torch.tensor(ls)))
			else:
				ten = torch.cat((ten, torch.tensor(ls).view(1, -1)), dim=0)
	return ten 


#################
# Network stuff
#################

class GravNet(nn.Module):
	def __init__(self, input_size, num_layers, hidden_size, output_size):
		super(GravNet, self).__init__()
		self.input_size = input_size
		self.output_size = output_size
		self.input_layer = nn.Linear(input_size, hidden_size)
		self.linear_layers = nn.ModuleList([nn.Linear(hidden_size, hidden_size)])
		self.linear_layers.extend([nn.Linear(hidden_size, hidden_size) for i in range(num_layers-1)])
		self.output_layer = nn.Linear(hidden_size, output_size)

	def forward(self, inp):
		x = torch.tanh(self.input_layer(inp))
		for layer in self.linear_layers:
			x = torch.relu(layer(x))
		x = self.output_layer(x)
		return x

	def train_gravnet(self, window_size, rn, vn, an, m, dt=0.00006):
		all_rn = rn.clone()
		all_vn = vn.clone()
		rn_net = rn.clone()
		vn_net = vn.clone()
		an_net = an.clone()
		part_rnp1_net = rn_net + vn_net * dt 
		for i in range(window_size):
			#net_in = torch.cat((rn.view(1, self.output_size)[0], part_rnp1.view(1, self.output_size)[0], m, energy.view(1)), dim=0)
			net_in = torch.cat((rn_net.view(1, self.output_size)[0], part_rnp1_net.view(1, self.output_size)[0], m), dim=0)
			#print(net_in)
			pred = self(net_in)

			anp1 = -pred
			rnp1 = rn_net + vn_net * dt + an_net * dt**2 / 2.0
			vnp1 = vn_net + (an_net + anp1) * dt / 2.0

			rn_net = rnp1 
			vn_net = vnp1 
			an_net = anp1 
			part_rnp1 = rn_net + vn_net * dt
			if i == 0:
				all_rn = torch.stack((all_rn, rn_net))
			else:
				all_rn = torch.cat((all_rn, rn_net.view(1, -1)), dim=0) 

		#resulting all_rn, all_vn is a matrix of all of the frames predicted by the net:
		"""
		[
		[frame1....],
		[frame2....],
		...
		]
		"""
		return all_rn, all_vn

#info on making pytorch loss functions at the bottom of this post
#https://spandan-madan.github.io/A-Collection-of-important-tasks-in-pytorch/
class Gravnet_loss(torch.nn.Module):
	def __init__(self):
		super(Gravnet_loss, self).__init__()

	def forward(self, pred_rn, pred_vn, exact_rn, exact_vn):
		#The loss here is the absolute error between the expected position and velocity
		# and their predicted counterparts both after a window of execution

		l1 = torch.norm(exact_rn - pred_rn, p=1)
		l2 = torch.norm(exact_vn - pred_vn, p=1)
		return l1 + l2

#########
# Main
#########

def main():
	n_p = 20
	#Cuda will save a lot of time. 17min per epoch on cpu vs 12sec on gpu on Carnie.
	device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
	#device = torch.device("cpu")
	#print('loading data...')
	datadir = f'./data{n_p}'
	sim_dirs = glob.glob(f'{datadir}/sim*')

	#Input items: position, partial position, masses, total energy
	#input_size = (n_p * 2) * 2 + n_p + 1
	input_size = (n_p * 2) * 2 + n_p
	#Output is just the forces to add to each particle
	output_size = n_p * 2
	hidden_size = 50
	num_layers = 4
	n_epochs = 50
	print_every = 1
	lr = 1e-6
	batch_size = 100
	window_size = 32
	net = GravNet(input_size, num_layers, hidden_size, output_size).to(device)
	#info on pytorch amsgrad: https://github.com/wikaiqi/AMSGradpytorch
	optimizer = torch.optim.Adam(net.parameters(), lr=lr, amsgrad=True) #The paper uses amsgrad so trying it out here
	criterion = Gravnet_loss()
	#criterion = Gravnet_energy_loss()
	#criterion = torch.nn.MSELoss()

	#Set up a directory for models. These include checkpoints and the final model
	modelsdir = './models'
	if not os.path.exists(f'{modelsdir}'):
		os.makedirs(f'{modelsdir}')

	print("Begin training")
	losses = []
	report_epochs = []
	start = time.time()
	for epoch in range(n_epochs):
		loss = 0
		for dir_index in np.random.permutation(len(sim_dirs)): #shuffle the sim to draw data from
			#load in the data for the window
			rns = file_to_tensor(f'{sim_dirs[dir_index]}/rn.txt').to(device)
			vns = file_to_tensor(f'{sim_dirs[dir_index]}/vn.txt').to(device)
			ans = file_to_tensor(f'{sim_dirs[dir_index]}/an.txt').to(device)
			m = file_to_tensor(f'{sim_dirs[dir_index]}/m.txt').to(device)
			for i in range(batch_size):
				start_index = random.randrange(len(rns) - (window_size + 1))
				end_index = start_index + window_size
				rn = Variable(rns[start_index])
				end_rn = Variable(rns[start_index:end_index])
				vn = Variable(vns[start_index])
				end_vn = Variable(vns[start_index:end_index])
				an = ans[start_index]
				#train over the window
				pred_rn, pred_vn = net.train_gravnet(window_size-1, rn, vn, an, m)

				#calculate and propagate the loss for this window
				loss += criterion(pred_rn, pred_vn, end_rn, end_vn)
		loss.backward()
		optimizer.step()
		optimizer.zero_grad()

		if epoch % print_every == 0:
			losses.append(loss.item())
			report_epochs.append(epoch)
			print(f"[{time_since(start)} epoch: ({epoch} : {n_epochs-1}) loss: {loss.item()}")
			torch.save(net.state_dict(), f'{modelsdir}/gravnet_chkpt_e{epoch}_{n_p}.pt')


	torch.save(net.state_dict(), f'{modelsdir}/gravnet_model_{n_p}.pt')
	plt.figure()
	plt.plot(report_epochs, losses, label='Loss')
	plt.title('Loss during training')
	plt.xlabel('Epoch')
	plt.ylabel('Loss')
	plt.legend()
	plt.savefig(f'gravnet_loss_{n_p}.png')


if __name__ == '__main__':
	main()
