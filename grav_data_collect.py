import numpy as np 
import os
#import pygame
#from pygame.locals import *

"""
So the idea behind this script is to be able to collect frame data
to train a neural net for a simple n-body gravity simulation. 

Each row represents a particle and with this matrix we can solve
the verlet integration in matrix form for the linear terms.
X_n = X_n-1 + (X_n-1 - X_n-2)
This leaves the non-linear force terms for the complete verlet 
integration out like so
X_n = X_n-1 + (X_n-1 - X_n-2) + h^2 M^-1 f_n-1
The neural networks job is to approximate that force term so that
the verlet integration is completely linear saving a lot of 
computation time. 

"""

class Particles(object):
	def __init__(self, num_parts=100, dim=2, dt=0.00006, screen_dim=500):
		self.screen_dim = screen_dim
		self.n = num_parts
		self.d = dim
		self.dt = dt
		#Particle matricies. Each row represents a particle
		#to squish into a vector use np.reshape(n * d, 1)
		self.parts_rn = np.random.rand(num_parts, dim)/2. + 0.25 #particles position at step n
		self.parts_vn = np.random.rand(num_parts, dim) #particles velocity at step n
		self.parts_an = np.random.rand(num_parts, dim) #particles acceleration at step n
		self.parts_m = np.random.rand(num_parts, 1) * 5
		self.parts_rnp1 = np.zeros((num_parts, dim)) #particles position at step n+1
		self.parts_rnp1_partial = self.parts_rn + self.parts_vn * self.dt  #The partial computation of the updated position
		self.parts_vnp1 = np.zeros((num_parts, dim))
		self.parts_anp1 = np.zeros((num_parts, dim))
		self.parts_rnm1 = self.parts_rn #particles position at step n-1
		
		#initialize energies
		self.K = np.zeros(1) #Kinetic energy for system
		self.W = np.zeros(1) #Potential energy for system
		for i, a in enumerate(self.parts_rn):
			for j, b in enumerate(self.parts_rn):
				if i == j:
					continue
				else:
					rij = a - b 
					self.W += self.parts_m[i] * self.parts_m[j] / np.linalg.norm(rij)

		#Kinetic energy
		#K = 0.5 * np.sum(np.dot(self.parts_m, np.linalg.norm(self.parts_vnp1, axis=1).reshape(1, self.n)))
		for i in range(self.n):
			self.K += self.parts_m[i] * np.linalg.norm(self.parts_vn[i])**2
		self.K *= 0.5
		#Potential Energy
		self.W *= -0.5 

	def calc_force(self):
		#G = 1.0 # Physics has been altered to pixel scale.
		forces = np.zeros((self.n, self.d))
		for i, a in enumerate(self.parts_rn):
			for j, b in enumerate(self.parts_rn):
				if i == j:
					continue
				else:
					rij = a - b 
					forces[i] += self.parts_m[i] * self.parts_m[j] / np.linalg.norm(rij)**2 * rij
		return forces

	def update_verlet(self):
		forces = self.calc_force()

		self.parts_anp1 = -forces / self.parts_m 
		self.parts_rnp1 = self.parts_rn + self.parts_vn * self.dt + self.parts_an * self.dt**2 / 2.0
		self.parts_vnp1 = self.parts_vn + (self.parts_an + self.parts_anp1) * self.dt / 2.0
 
		self.parts_rn = self.parts_rnp1
		self.parts_vn = self.parts_vnp1
		self.parts_an = self.parts_anp1

# no energy calculations just yet. Takes too much time for where I am at currently
	def update_with_energy(self):
		#G = 1.0 # Physics has been altered to pixel scale.
		# Update forces
		forces = np.zeros((self.n, self.d))
		self.K = 0 #Kinetic energy for system
		self.W = 0 #Potential energy for system
		for i, a in enumerate(self.parts_rn):
			for j, b in enumerate(self.parts_rn):
				if i == j:
					continue
				else:
					rij = a - b 
					forces[i] += self.parts_m[i] * self.parts_m[j] / np.linalg.norm(rij)**2 * rij
					self.W += self.parts_m[i] * self.parts_m[j] / np.linalg.norm(rij)

		#Kinetic energy
		#K = 0.5 * np.sum(np.dot(self.parts_m, np.linalg.norm(self.parts_vnp1, axis=1).reshape(1, self.n)))
		for i in range(self.n):
			self.K += self.parts_m[i] * np.linalg.norm(self.parts_vn[i])**2
		self.K *= 0.5
		#Potential Energy
		self.W *= -0.5 

		"""
		# Update r, v, a using verlet integration
		# this is left here to help explain the logic. Each row is a particle and
		# this loop then updates each one individually. Because python for loops
		# are slow we just use a numpy matrix calculation
		for i in range(self.n):
			self.parts_anp1[i] = -forces[i]
			self.parts_rnp1[i] = self.parts_rn[i] + self.parts_vn[i] * self.dt + self.parts_an[i] * self.dt**2 / 2.0
			self.parts_vnp1[i] = self.parts_vn[i] + (self.parts_an[i] + self.parts_anp1[i]) * self.dt / 2 

			self.parts_rn[i] = self.parts_rnp1[i]
			self.parts_vn[i] = self.parts_vnp1[i]
			self.parts_an[i] = self.parts_anp1[i]
		"""
		# Update r, v, a using matrix form verlet integration
		self.parts_anp1 = -forces
		self.parts_rnp1 = self.parts_rn + self.parts_vn * self.dt + self.parts_an * self.dt**2 / 2.0
		self.parts_vnp1 = self.parts_vn + (self.parts_an + self.parts_anp1) * self.dt / 2.0

		self.parts_rnm1 = self.parts_rn 
		self.parts_rn = self.parts_rnp1
		self.parts_vn = self.parts_vnp1
		self.parts_an = self.parts_anp1
		self.parts_rnp1_partial = self.parts_rn + self.parts_vn * self.dt 

"""

	def draw(self, surface):
		for i in range(self.n):
			pygame.draw.circle(
				surface,
				(255,255,255), #color
				(int(round(self.parts_rn[i, 0])) ,int(round(self.parts_rn[i, 1]))), #position
				1, #Radius
				0 #Not sure
				)

def get_input():
	keys_pressed = pygame.key.get_pressed()
	mouse_buttons = pygame.mouse.get_pressed()
	mouse_position = pygame.mouse.get_pos()
	mouse_rel = pygame.mouse.get_rel()
	for event in pygame.event.get():
		if   event.type == QUIT: return False
		elif event.type == KEYDOWN:
			if   event.key == K_ESCAPE: return False
	return True
"""
def main():
	n_p = 20
	nsims = 200 # number of unique systems to collect data on. The more the better I think
	nframes = 1000 #number of frames in a sim. Should have enough to give a good representation of the system	

	screen_dim = 800
	screen_size = [screen_dim, screen_dim]
	"""
	# Display without collecting loop
	pygame.display.init()
	pygame.font.init()
	icon = pygame.Surface((1,1)); icon.set_alpha(0); pygame.display.set_icon(icon)
	pygame.display.set_caption("Gravity Simulation")
	surface = pygame.display.set_mode(screen_size)

	clock = pygame.time.Clock()
	while True:
		if not get_input(): break
		p.update()
		#draw
		surface.fill((25,0,0))
		p.draw(surface)
		pygame.display.flip()
		clock.tick(60.)
	pygame.quit()
	"""

	savedir = f'./data{n_p}'
	if not os.path.exists(savedir):
		os.makedirs(f'{savedir}')
	for n in range(nsims):
		print(f'working on sim {n}')
		sim_data_dir = f'{savedir}/sim{n}'
		if not os.path.exists(sim_data_dir):
			os.makedirs(sim_data_dir)
		p = Particles(num_parts=n_p, screen_dim=screen_dim)
		#only need a one line file for the mass. no need to update this every frame
		with open(f'{sim_data_dir}/m.txt', 'w') as mass_file:
			np.savetxt(mass_file, p.parts_m, newline=',')
			mass_file.write('\n')
		#Open all the files! Not sure if there is a better way to do this...
		with open(f'{sim_data_dir}/rn.txt', 'w') as rn_file, open(f'{sim_data_dir}/vn.txt', 'w') as vn_file,\
		open(f'{sim_data_dir}/an.txt', 'w') as an_file:
			for i in range(nframes):
				np.savetxt(rn_file, p.parts_rn.reshape(1, n_p * 2)[0], newline=',')
				rn_file.write('\n')
				np.savetxt(vn_file, p.parts_vn.reshape(1, n_p * 2)[0], newline=',')
				vn_file.write('\n')
				np.savetxt(an_file, p.parts_an.reshape(1, n_p * 2)[0], newline=',')
				an_file.write('\n')
				#Update particles
				p.update_verlet()
	

if __name__ == '__main__':
	main()