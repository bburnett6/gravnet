import numpy as np 
import os
import pygame
from pygame.locals import *
import copy

from train_gravnet import GravNet 

import torch

"""
Sources
# Gravity stuff
http://www.scholarpedia.org/article/N-body_simulations_(gravitational)
http://www.cs.ucy.ac.cy/~ppapap01/nbody/Presentation.pdf

# Speeding up pytorch
https://towardsdatascience.com/speed-up-your-algorithms-part-1-pytorch-56d8a4ae7051
https://sagivtech.com/2017/09/19/optimizing-pytorch-training-code/

# The Paper
http://theorangeduck.com/media/uploads/other_stuff/deep-cloth-paper.pdf
"""
class Particles(object):
	def __init__(self, net_path=None, num_parts=100, dim=2, dt=0.00006, screen_dim=500):
		self.screen_dim = screen_dim
		self.n = num_parts
		self.d = dim
		self.dt = dt
		#Particle matricies. Each row represents a particle
		#to squish into a vector use np.reshape(n * d, 1)
		self.parts_rn = np.random.rand(num_parts, dim)/2. + 0.25 #particles position at step n
		self.parts_vn = np.random.rand(num_parts, dim) #particles velocity at step n
		self.parts_an = np.random.rand(num_parts, dim) #particles acceleration at step n
		self.parts_m = np.random.rand(num_parts, 1) * 5
		self.parts_rnp1 = np.zeros((num_parts, dim)) #particles position at step n+1
		self.parts_rnp1_partial = self.parts_rn + self.parts_vn * self.dt  #The partial computation of the updated position
		self.parts_vnp1 = np.zeros((num_parts, dim))
		self.parts_anp1 = np.zeros((num_parts, dim))
		self.parts_rnm1 = self.parts_rn #particles position at step n-1
		
		#initialize energies
		self.K = np.zeros(1) #Kinetic energy for system
		self.W = np.zeros(1) #Potential energy for system
		for i, a in enumerate(self.parts_rn):
			for j, b in enumerate(self.parts_rn):
				if i == j:
					continue
				else:
					rij = a - b 
					self.W += self.parts_m[i] * self.parts_m[j] / np.linalg.norm(rij)

		#Kinetic energy
		#K = 0.5 * np.sum(np.dot(self.parts_m, np.linalg.norm(self.parts_vnp1, axis=1).reshape(1, self.n)))
		for i in range(self.n):
			self.K += self.parts_m[i] * np.linalg.norm(self.parts_vn[i])**2
		self.K *= 0.5
		#Potential Energy
		self.W *= -0.5 

		if net_path:
			device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
			self.gravnet = GravNet((self.n * 2) * 2 + self.n, 4, 50, num_parts * dim)
			self.gravnet.load_state_dict(torch.load(net_path, map_location=device))
			self.gravnet.eval()
		else:
			self.gravnet = None

	def update_classic(self):
		#G = 1.0 # Physics has been altered to pixel scale.
		# Update forces
		forces = np.zeros((self.n, self.d))
		self.K = 0 #Kinetic energy for system
		self.W = 0 #Potential energy for system
		for i, a in enumerate(self.parts_rn):
			for j, b in enumerate(self.parts_rn):
				if i == j:
					continue
				else:
					rij = a - b 
					forces[i] += self.parts_m[i] * self.parts_m[j] / np.linalg.norm(rij)**2 * rij
					#self.W += self.parts_m[i] * self.parts_m[j] / np.linalg.norm(rij)

		#Kinetic energy
		#K = 0.5 * np.sum(np.dot(self.parts_m, np.linalg.norm(self.parts_vnp1, axis=1).reshape(1, self.n)))
		#for i in range(self.n):
		#	self.K += self.parts_m[i] * np.linalg.norm(self.parts_vn[i])**2
		self.K *= 0.5
		#Potential Energy
		self.W *= -0.5 

		"""
		# Update r, v, a using verlet integration
		# this is left here to help explain the logic. Each row is a particle and
		# this loop then updates each one individually. Because python for loops
		# are slow we just use a numpy matrix calculation
		for i in range(self.n):
			self.parts_anp1[i] = -forces[i]
			self.parts_rnp1[i] = self.parts_rn[i] + self.parts_vn[i] * self.dt + self.parts_an[i] * self.dt**2 / 2.0
			self.parts_vnp1[i] = self.parts_vn[i] + (self.parts_an[i] + self.parts_anp1[i]) * self.dt / 2 

			self.parts_rn[i] = self.parts_rnp1[i]
			self.parts_vn[i] = self.parts_vnp1[i]
			self.parts_an[i] = self.parts_anp1[i]
		"""
		# Update r, v, a using matrix form verlet integration
		self.parts_anp1 = -forces
		self.parts_rnp1 = self.parts_rn + self.parts_vn * self.dt + self.parts_an * self.dt**2 / 2.0
		self.parts_vnp1 = self.parts_vn + (self.parts_an + self.parts_anp1) * self.dt / 2.0

		self.parts_rnm1 = self.parts_rn 
		self.parts_rn = self.parts_rnp1
		self.parts_vn = self.parts_vnp1
		self.parts_an = self.parts_anp1
		self.parts_rnp1_partial = self.parts_rn + self.parts_vn * self.dt 

	def update_net(self):
		if not self.gravnet:
			self.update_classic()
			return
		self.parts_rnp1 = self.parts_rn + self.parts_vn * self.dt + self.parts_an * self.dt**2 / 2.0
		self.parts_vnp1 = self.parts_vn + self.parts_an * self.dt / 2.0

		#There is most likely a better way of setting up the network input, but here is this.
		net_in = []
		net_in.append(self.parts_rn.reshape(1, self.n * self.d)[0]) #the [0]'s are because the reshape returns a [[data]] array and we only want [data]
		net_in.append(self.parts_rnp1_partial.reshape(1, self.n * self.d)[0])
		net_in.append(self.parts_m.reshape(1, self.n)[0])
		#net_in.append(self.K + self.W)
		net_in = np.hstack(net_in).reshape(1, self.gravnet.input_size)[0]
		#in_min, in_max = np.min(net_in), np.max(net_in)
		#in_min, in_max = -55742325.35668938, 54115852.51283884
		#in_min, in_max = -54956196.46804544, 53177899.832912065
		#net_in = (net_in - in_min) / (in_max - in_min) #normalize input
		net_out = self.gravnet(torch.tensor(net_in).float()).detach().numpy()
		#net_out = (net_out * (in_max - in_min)) + net_out #denormalize output
		self.parts_anp1 = -net_out.reshape(self.n, self.d)
		self.parts_vnp1 += self.parts_anp1 * self.dt / 2.0

		self.parts_rnm1 = self.parts_rn 
		self.parts_rn = self.parts_rnp1
		self.parts_vn = self.parts_vnp1
		self.parts_an = self.parts_anp1
		self.parts_rnp1_partial = self.parts_rn + self.parts_vn * self.dt 
		self.W = 0
		self.K = 0
		"""
		for i, a in enumerate(self.parts_rn):
			for j, b in enumerate(self.parts_rn):
				if i == j:
					continue
				else:
					rij = a - b 
					self.W += self.parts_m[i] * self.parts_m[j] / np.linalg.norm(rij)

		for i in range(self.n):
			self.K += self.parts_m[i] * np.linalg.norm(self.parts_vn[i])**2
		"""
		self.K *= 0.5
		#Potential Energy
		self.W *= -0.5 

	def draw(self, surface, color='white'):
		for i in range(self.n):
			pygame.draw.circle(
				surface,
				(255,255,255) if color == 'white' else (255, 0, 0), #color
				(int(round(self.parts_rn[i, 0] * self.screen_dim)) ,int(round(self.parts_rn[i, 1] * self.screen_dim))), #position
				1, #Radius
				0 #Not sure
				)

def get_input():
	keys_pressed = pygame.key.get_pressed()
	mouse_buttons = pygame.mouse.get_pressed()
	mouse_position = pygame.mouse.get_pos()
	mouse_rel = pygame.mouse.get_rel()
	for event in pygame.event.get():
		if   event.type == QUIT: return False
		elif event.type == KEYDOWN:
			if   event.key == K_ESCAPE: return False
	return True


def main():
	n_p = 20
	screen_dim = 800
	p_classic = Particles(net_path=f'./models/gravnet_model_{n_p}.pt', num_parts=n_p, screen_dim=screen_dim)
	#p_classic = Particles(net_path=None, num_parts=n_p, screen_dim=screen_dim)
	p_net = copy.deepcopy(p_classic)
	screen_size = [screen_dim, screen_dim]
	pygame.display.init()
	pygame.font.init()
	icon = pygame.Surface((1,1)); icon.set_alpha(0); pygame.display.set_icon(icon)
	pygame.display.set_caption("Gravity Simulation")
	surface = pygame.display.set_mode(screen_size)

	clock = pygame.time.Clock()
	i = 0
	#while i <= 10:
	while True:
		if not get_input(): break
		print(i)
		p_classic.update_classic()
		p_net.update_net()
		"""
		if i % 2:
			p_net.update_net()
		else:
			p_net.update_classic()
		"""
		#draw
		surface.fill((25,0,0))
		p_classic.draw(surface)
		p_net.draw(surface, color='Red')
		pygame.display.flip()
		#clock.tick(5.)
		i+=1
	pygame.quit()

if __name__ == '__main__':
	main()